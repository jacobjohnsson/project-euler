import java.math.*;
import java.util.List;
import java.util.LinkedList;

class Main {
  public static void main(String[] args) {
    BigInteger n = new BigInteger("600851475143");
    List<BigInteger> factors = new LinkedList<BigInteger>();
    BigInteger sqrt = n.sqrt();

    for (BigInteger i = new BigInteger("2"); i.compareTo(sqrt) == -1; i = i.add(new BigInteger("1"))) {
      while (n.remainder(i).equals(new BigInteger("0"))) {
        System.out.println("Found: " + i);
        n = n.divide(i);
        System.out.println("New n: " + n);
        factors.add(i);
      }
    }

    BigInteger max = new BigInteger("0");
    for (BigInteger f : factors) {
      if (f.compareTo(max) == 1) {
        max = f;
      }
    }
    System.out.println("Max: " + max);
  }
}
