object Main {

  val number: BigInt = BigInt("600851475143")


  def factorize(number: BigInt): List[BigInt] = {
    def foo(x: BigInt, a: BigInt = 2, list: List[BigInt] = Nil): List[BigInt] = a*a > x match {
      case false if x % a == 0  => foo(x / a, a, a :: list )
      case false                => foo(x, a + 1, list)
      case true                 => x :: list
    }
    foo(number)
  }

  def main(args: Array[String]): Unit = {
    val factors: List[BigInt] = factorize(number);
    val factorsInt: List[Int] = factors.map(n => n.toInt)
    println(factorsInt.fold(0)((f1, f2) => f1 max f2))
  }
}
