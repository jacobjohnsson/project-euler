class Main {

  private final static int a = 3;
  private final static int b = 5;

  public static void main(String[] args) {
    int sum = 0;

    for (int i = 3; i < 1000; i++ ) {
      if (i % a == 0) {
        sum += i;
      } else if (i % b == 0) {
        sum += i;
      }
    }
    System.out.println(sum);
  }
}
