object Main {
  def main(args: Array[String]) = println((3 until 1000).filter(i => (i % 3 == 0) || (i % 5 == 0)).sum);
}
