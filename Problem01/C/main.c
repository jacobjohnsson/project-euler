#include <stdio.h>

int main() {
  int sum = 0;
  for (size_t i = 0; i < 1000; i++) {
    if (i % 3 == 0) {
      sum += i;
    } else if (i % 5 == 0) {
      sum += i;
    }
  }

  printf("%d\n", sum);
}
